# GitLab CI/CD Setup and Usage Guide

This guide provides instructions on setting up and using the GitLab CI/CD pipeline for your project.

## Table of Contents

- [Prerequisites](#prerequisites)
- [GitLab CI/CD Workflow](#gitlab-cicd-workflow)
- [Setting Environment Variables](#setting-environment-variables)
- [Building and Deploying Microservices](#building-and-deploying-microservices)
  - [Building Microservices](#building-microservices)
  - [Deploying Microservices](#deploying-microservices)
- [Deployment on Kubernetes Cluster](#deployment-on-kubernetes-cluster)

## Prerequisites

Before you begin, make sure you have the following prerequisites in place:

- Access to a GitLab repository for your project.
- Docker installed on your local machine.
- SSH private key for server access (if applicable).
- Kubernetes configuration (kubeconfig) file (if using Kubernetes).

## GitLab CI/CD Workflow

Your GitLab CI/CD pipeline follows the following workflow:

1. Changes are pushed to your GitLab repository.
2. GitLab CI/CD automatically triggers pipelines based on the defined rules and conditions.
3. The pipeline includes two stages: `build` and `deploy`.
4. The `build` stage builds Docker images for microservices when there are changes in corresponding directories.
5. The `deploy` stage deploys microservices based on changes to Kubernetes manifests and other configurations.

## Setting Environment Variables

Before running your GitLab CI/CD pipeline, make sure to set the following environment variables in your GitLab CI/CD project settings:

- `CI_REGISTRY_IMAGE`: The Docker registry image name.
- `CI_REGISTRY_USER` and `CI_REGISTRY_PASSWORD`: Credentials for Docker registry access.
- `DEV_ENDPOINT`, `STAGING_ENDPOINT`, and `PROD_ENDPOINT`: URLs for different environments.
- `DEV_SERVER_HOST`, `STAGING_SERVER_HOST`, and `PROD_SERVER_HOST`: Host addresses for different environments.
- `SSH_PRIVATE_KEY`: Your SSH private key for server access (if applicable).
- `KUBE_CONFIG`: Path to your Kubernetes configuration file (kubeconfig).

## Building and Deploying Microservices

### Building Microservices

To build microservices, GitLab CI/CD uses Docker. Here's how to trigger the build process for specific microservices:

#### Build Frontend

```bash
$ git push origin <your-branch>  # Trigger the CI/CD pipeline


## Deployment on Kubernetes Cluster

To deploy microservices on a Kubernetes cluster, GitLab CI/CD uses Kubernetes manifests and configurations.

#### Deploy Frontend on Kubernetes

To deploy the Frontend microservice on a Kubernetes cluster, use the following command:

```bash
$ git push origin <your-branch>  # Trigger the CI/CD pipeline


Microservices project made up of 1 frontend service and 2 backend services: products & shopping-cart

#### To build the projects
    docker build -t ms-frontend:1.0 frontend
    docker build -t ms-products:1.0 products
    docker build -t ms-shopping-cart:1.0 shopping-cart

#### To start them locally (repeat for each micro service)
    cd micro-service-name 
    npm install
    npm run
    

#### To start them as docker containers - separate commands
    docker run -d -p 3000:3000 \
    -e PRODUCTS_SERVICE=host.docker.internal \
    -e SHOPPING_CART_SERVICE=host.docker.internal \
    ms-frontend:1.0

    docker run -d -p 3001:3001 ms-products:1.0
    docker run -d -p 3002:3002 ms-shopping-cart:1.0

#### To start with docker-compose (repeat for each micro service)
    export COMPOSE_PROJECT_NAME=micro-service-name (frontend)
    export DC_IMAGE_NAME=micro-service-image-name (ms-frontend)
    export DC_IMAGE_TAG=tag (1.0)
    export DC_APP_PORT=port (3000)

    docker-compose -d up
